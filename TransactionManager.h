//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//

#ifndef ADB_TRANSCTIONMANAGER_H
#define ADB_TRANSCTIONMANAGER_H

 
#include "DataManager.h"

using namespace std;

class TransactionManager {
private:
    s_time * time_ptr;
	DataManager * DM;
    map<I_id, vector<S_id> > item_sites;
    map<S_id, vector<I_id> > site_items;
    map<S_id, bool> site_status; // true means up

    int transCounter;
    vector<Transaction> trans;    
    map<T_id, int> trans_index;

    map<S_id, set<T_id> > site_trans;
    map<I_id, deque<pair<T_id, bool> > > transQue; // true means isRead
    map<I_id, vector<T_id> > ROtransQue;

     
public:
    TransactionManager(s_time *, DataManager *);
    void begin(T_id);
    void beginRO(T_id);
    void addNewTrans(T_id, bool); // true = RO
    void takeSnapshot(T_id);

    void end(T_id);
    void fail(S_id);
    void recover(S_id);

    void abort(T_id, bool sitefail); // true due to site failure; false due to wait_die
    void abortWaitingTrans(T_id); // only due to site failure; 
    void commit(T_id);
    void read(Operation & operation);
    void write(Operation & operation);

    void getTranstatus() const;

    bool shouldWait(T_id, I_id, bool isRead) const; 
    bool shouldWait(T_id, bool isRead1, T_id, bool isRead2) const;
    // void completeRW(Operation & operation);

    void getWLocks(T_id, I_id);
    bool getRLock(T_id, I_id); // true means got one
    set<I_id> releaseLockNLog(T_id);
    vector<pair<T_id, bool> > getAllTrans(I_id) const;

    
    void enque(Operation & operation);
    void activateQue(I_id);

    void enqueRO(Operation & operation);
    void clearROQue(S_id, I_id);
    

    S_id hasSiteUp(I_id) const; // if no site is up, return -1

    bool hasSiteAccess(T_id, I_id) const;
    S_id findSiteAccess(T_id, I_id) const;

    bool hasWritten(T_id, I_id) const;
    bool hasAllWLocks(T_id, I_id) const;

    bool isReplicated(I_id) const;

    
    void setReadLog(T_id, I_id, S_id);
    void setWriteLog(T_id, I_id, S_id);
    

    void printRead(Operation &, S_id);
    void recordWrite(T_id, I_id, Val);

   
    Val getSnapshotVal(T_id, I_id) const;
    Val getWVal(T_id, I_id) const;

    bool isActive(T_id) const;
    bool isWaiting(T_id) const;
    bool isAborted(T_id) const;
    bool isCommitted(T_id) const;
    bool isRO(T_id) const;
    s_time getStartTime(T_id)const;
    int getIndex(T_id) const;


    void addSite_trans(S_id, T_id);
    void removeSite_trans(S_id, T_id);    
};


#endif //ADB_TRANSCTIONMANAGER_H
