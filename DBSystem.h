//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//

#ifndef ADB_DBSYSTEM_H
#define ADB_DBSYSTEM_H

 
#include "TransactionManager.h"
  

using namespace std;

class DBSystem {
private:
    s_time currtime;
    DataManager DM; 
    TransactionManager  TM;
    

public:
    DBSystem(vector<Site> &, map<S_id, int> &, vector<Item> &, map<I_id, int> &, map<I_id, vector<S_id> > &);

    void handleCommands(string line);
    void runCommand(string command);
    Optype identifyOperationType (string op);

    void beginTrans(string command);
    void beginROTrans(string command);
    void read(string command);
    void write(string command);
    void endTrans(string command);
    void dump(string command) const;
    void fail(string command);
    void recover(string command);

    void queryTranstatus() const;
    void queryDBstatus() const;
    
    string trim(string str) const;
    vector<string> lineToCommands(string line) const;
    int getNums (string command) const;
};


#endif //ADB_DBSYSTEM_H
