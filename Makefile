CC=g++

CFLAGS=-c -Wall -std=c++11

all: adb

adb: main.o DBSystem.o TransactionManager.o DataManager.o
	$(CC) main.o DBSystem.o TransactionManager.o DataManager.o -o adb

main.o: main.cpp DBSystem.h 
	$(CC) $(CFLAGS) main.cpp


DBSystem.o: DBSystem.cpp DBSystem.h TransactionManager.h
	$(CC) $(CFLAGS) DBSystem.cpp

TransactionManager.o: TransactionManager.cpp TransactionManager.h DataManager.h
	$(CC) $(CFLAGS) TransactionManager.cpp



DataManager.o: DataManager.cpp DataManager.h cons.h
	$(CC) $(CFLAGS) DataManager.cpp



clean: 
	rm -rf *o adb

