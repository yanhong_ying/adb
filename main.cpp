//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//



#include "DBSystem.h"


using namespace std;

vector<Site> sites;
map<S_id, int> site_index;
vector<Item> data;
map<I_id, int> item_index;
map<I_id, vector<S_id> > item_sites;

void initial();

/* 
 * main funtion. create whole database system and execute
 */
int main(int argc, char ** argv) {
	initial();
	DBSystem db(sites, site_index, data, item_index, item_sites);
	
    if(argc < 2){
        cout << "Please input test file path\n";
        return -1;
    }
     
    ifstream file(argv[1]);
    string line;
    if (file.is_open()) {
        while(getline(file, line)) {
            db.handleCommands(line);
        }
        file.close();
    }else{
        cout << "Unable to open "<< argv[1] << "file \n";
    }
    // db.queryTranstatus();
    // cout << endl;
    // db.queryDBstatus();
    // cout << "************** end ************" << endl;
    // cout << endl;

    return 0;
}

/*
 * initiate data in database.
 * even number in every site
 * odd number in one site 
 */
void initial() {
	 for(int i = 1; i  <= 10; i++){
        Site s;
        s.site_id = i;
        s.isUp = true;
        s.siteLog.push_back(pair<s_time, bool>(0, true));
        sites.push_back(s);
        site_index[i] = i - 1;
    }
     
    for(int j = 1; j <= 20; j++){
        Item it;
        it.item_id = j;
        it.records.push_back(pair<s_time, int>(0, 10 * j));
        data.push_back(it);
        item_index[j] = j - 1;
        
        vector<S_id> s_ids;
        if (j % 2 == 1) {
            S_id site_id = (j+1)%10 == 0 ? 10 : (j+1)%10;
            s_ids.push_back(site_id); 
            sites[site_id - 1].items.insert(j);
        }else {
            for (int i = 1; i <= 10; i++) {
                s_ids.push_back(i);
                sites[i - 1].items.insert(j);
                sites[i - 1].rep_items[j] = std::pair<s_time, bool> (0, true);
            }
        }
        item_sites[j] = s_ids;    
    }
}