//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//

#ifndef ADB_CONS_H
#define ADB_CONS_H

#include <fstream>
#include <iostream>
#include <unordered_map>
#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>
#include <queue>
#include <deque>
#include <stdlib.h> // atoi
#include <ctype.h> //isdigit

using namespace std;

typedef int S_id;
typedef int T_id;
typedef int I_id;
typedef int s_time;
typedef int Val;
typedef pair<s_time, Val> PTV;

enum Optype {BEGIN, BEGINRO, R, W, END, DUMP, FAIL, RECOVER, INVALID};
enum Transtatus {ACTIVE, WAITING, COMMITTED, SITEFAIL, WAITDIE, ELSE};

typedef struct Lock {
    bool isRLock;
    T_id t_id;
    s_time lockTime;
}Lock;


struct Item{
    I_id item_id;
    deque <PTV> records;
};


typedef struct Operation{
    bool isRead;
    T_id t_id;
    I_id item_id;
    s_time start;
    Val  value;
}Operation;


typedef struct Site {
    S_id site_id;
    bool isUp;
    set<I_id> items;
    unordered_map<I_id, pair<s_time, bool> > rep_items;  // true means readable
    map<I_id, list<Lock> > lockTable;
    vector<pair<s_time, bool> > siteLog;  //pair<s_time, bool> if true (or false) it means  site is up (down) since s_time 
} Site;


typedef struct SiteAccess {
    bool isRead;
    S_id site_id;
    s_time initial_time;
}SiteAccess; 


typedef struct Transaction {
    T_id t_id;
    s_time start;
    s_time end;
    bool isRO;
    Transtatus status;   

    Operation pendingOp;
    map<I_id, list<SiteAccess> > access_log; // essentially a lock table from the point of view of transaction

    map<I_id, Val> write_or_snapshot; // for read-only, it is a snapshot; otherwise, it is a record of writes to be committed
} Transaction;

#endif //ADB_CONS_H
