//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//

#include "TransactionManager.h"

/*
 * construct TransactionManager
 */
TransactionManager::TransactionManager(s_time * time_ptr, DataManager * DM): time_ptr(time_ptr), DM(DM) {
	transCounter = 0;
	item_sites = DM->initial_item_sites();
	site_items = DM->initial_site_items();
	site_status = DM->initial_site_status();
}

/*
 * begin new transaction
 * add transaction to transaction list
 */
void TransactionManager::begin(T_id t_id) {
	if(trans_index.find(t_id) != trans_index.end()) {
		std::cout << "There already exists a transaction with the same ID!" <<endl;
		return;
	} else {
		trans_index[t_id] = transCounter;
		transCounter++;
		addNewTrans(t_id, false);
	} 
	
}

/*
 * begin new readonly transaction
 * add readonly transaction to transaction list
 */
void TransactionManager::beginRO(T_id t_id) {
	if(trans_index.find(t_id) != trans_index.end()) {
		std::cout << "There already exists a transaction with the same ID!" <<endl;
		return;
	} else {
		trans_index[t_id] = transCounter;
		transCounter++;
		addNewTrans(t_id, true);
	} 
}

/*
 * create new transaction object
 */
void TransactionManager::addNewTrans(T_id t_id, bool isRO) {
	Transaction newtrans;
	newtrans.t_id = t_id;
	newtrans.start = * time_ptr;
	newtrans.isRO = isRO;
	newtrans.status = ACTIVE;
	trans.push_back(newtrans);
	if(isRO) {
		takeSnapshot(t_id);
	}
}

/*
 * take snapshot for readonly transaction
 * 
 */
void TransactionManager::takeSnapshot(T_id t_id) {
	int i = getIndex(t_id);
	if(!trans[i].isRO) {
		return;
	}
	std::map<I_id, std::vector<S_id> >::iterator it = item_sites.begin();
	for(; it != item_sites.end(); it++) {
		I_id item_id = it->first;
		trans[i].write_or_snapshot[item_id] = DM->getValue(item_id, * time_ptr);
	}
}

/*
 * end transaction
 * if the transaction is active, commit the transaction.
 * is the transaction is waiting, abort it
 */
void TransactionManager::end(T_id t_id){
	if(trans_index.find(t_id) == trans_index.end()) {
		std::cout << "There does not exist a transaction with this ID!" <<endl;
		return;
	}
	if(isActive(t_id)) {
		commit(t_id);
	}

	if(isWaiting(t_id)){
		std::cout << "Transcation " << t_id << " is waiting and aborted due to end operation" << endl;
		abortWaitingTrans(t_id);
	}
}

/*
 * fail one site
 * bort all transactions have lcoks or waiting some resource on this site
 */
void TransactionManager::fail(S_id site_id) {
	std::cout << "Site " << site_id << " failed" << endl;
	site_status[site_id] = false;
	if(site_trans.empty() || site_trans.find(site_id) == site_trans.end())
		return;

	std::set<T_id>  t_ids = site_trans[site_id];
	std::set<T_id>::iterator it = t_ids.begin();
	for(; it != t_ids.end(); it++) {
		T_id t_id = *it;
		if(isWaiting(t_id)) {
			abortWaitingTrans(t_id);
		}else abort(t_id, true);
		// std::cout << "back in fail" << endl;
	}

	if(!site_trans.empty() || site_trans.find(site_id) != site_trans.end()){
		site_trans.erase(site_id);
	}
}

/*
 * recover failed site
 * recovered site can't be read, but can be write
 * after the first write operation, site can be read
 */
void TransactionManager::recover(S_id site_id) {
	std::cout << "Site " << site_id << " recovered" << endl;
	site_status[site_id] = true;
	if(site_items.find(site_id) == site_items.end())
		return;
	std::vector<I_id> & items = site_items[site_id];
	for(int i = 0; i < (int)items.size(); i++) {
		I_id item_id = items[i];
		clearROQue(site_id, item_id);

		if(transQue.find(item_id) == transQue.end() || transQue[item_id].empty())
			continue;
		std::deque<std::pair<T_id, bool> > & transInline = transQue[item_id];

		if(isReplicated(item_id) && transInline.front().second)  // Queue front is read operation; skip
			continue;
		activateQue(item_id);
	}

}

/*
 * abort transaction
 * release all locks holded by this transaction
 * active other transactions wait for the resource holded by this aborted one
 */
void TransactionManager::abort(T_id t_id, bool sitefail) { // true due to site failure; false due to wait_die
	int i = getIndex(t_id);
	trans[i].end = * time_ptr;
	std::cout << "T" << t_id << " aborted ";
	if(sitefail) {
		trans[i].status = SITEFAIL;
		std::cout << "due to site fail" << endl;
	} else {
		trans[i].status = WAITDIE;
		std::cout << "due to wait_or_die" << endl;
	} 

	std::set<I_id> items = releaseLockNLog(t_id); 

	// std::cout << "back in abort " << endl;
	std::set<I_id>::iterator it3 = items.begin();
	for(; it3 != items.end(); it3 ++) {
		activateQue(*it3);
	}	 
	// std::cout << "still in abort" << endl;
}

/*
 * abort waiting transactions 
 */
void TransactionManager::abortWaitingTrans(T_id t_id) {
	int i = getIndex(t_id);
	trans[i].end = * time_ptr;
	I_id item_id = trans[i].pendingOp.item_id;

	if(transQue.empty() ||transQue.find(item_id) == transQue.end() || transQue[item_id].empty())
		return;
	std::deque<std::pair<T_id, bool> > & transInline = transQue[item_id];
	std::deque<std::pair<T_id, bool> > :: iterator it = transInline.begin();

	for(; it != transInline.end(); ) {
		if(it->first == t_id) {
			it = transInline.erase(it);
		} else it++;
	}
	abort(t_id, true);
}

/*
 * commit transaction
 * release all locks hold by commited transactions
 * active all transactions waited for this commited transactions
 */   
void TransactionManager::commit(T_id t_id){
	std::cout << "T" << t_id << " committed" << endl;
	int index = getIndex(t_id);
	trans[index].status = COMMITTED;
	trans[index].end = * time_ptr;
	if(trans[index].isRO)
		return;
	std::map<I_id, Val> & write_or_snapshot = trans[index].write_or_snapshot;
	std::map<I_id, Val>::iterator it1 = write_or_snapshot.begin();
	for(; it1 != write_or_snapshot.end(); it1++) {
		I_id item_id = it1->first;
		DM->addCommit(item_id, it1->second);
		if(!isReplicated(item_id)) {
			continue;
		}
		std::list<SiteAccess> & access = trans[index].access_log[item_id];
		std::list<SiteAccess>::iterator it2 = access.begin();
		for(; it2 != access.end(); it2++) {
			if(!it2->isRead) {
				DM->reactivateRep(it2->site_id, item_id);
			}	
		}
	}

	std::set<I_id> items = releaseLockNLog(t_id); 
	std::set<I_id>::iterator it2 = items.begin();
	for(; it2 != items.end(); it2 ++) {
		activateQue(*it2);
	}
}

/*
 * read Item from database
 * if its a readonly operation. read from snapshot
 * if not, try to get read lock for this Item of one site 
 * if get lock, print result
 * if cannot get lock, use wait-die protocol
 */
void TransactionManager::read(Operation & operation) {
	T_id t_id = operation.t_id;
	I_id item_id = operation.item_id;

	if(!isActive(t_id))
		return;

	if(isRO(t_id)) {
		S_id site_id =  hasSiteUp(item_id);

		if (site_id != -1) {
			operation.value = getSnapshotVal(t_id, item_id);
			printRead(operation, site_id);
			
		} else enqueRO(operation);
		return;
	}

	if(hasWritten(t_id, item_id)) {
		operation.value = getWVal(t_id, item_id);
		S_id site_id = findSiteAccess(t_id, item_id);
		printRead(operation, site_id);
		return;
	} 
	 
	if (hasSiteAccess(t_id, item_id) || getRLock(t_id, item_id)) {
		operation.value = DM->getValue(item_id, * time_ptr);
		S_id site_id = findSiteAccess(t_id, item_id);
		printRead(operation, site_id);
		return;
	}
	
	if (shouldWait(t_id, item_id, true)) { // true means read
		enque(operation);
	} else abort(t_id, false); //false means wait_die

}

/*
 * write Item value to database
 * try to get write locks of this item from all sites
 * if get all locks,change the value when commit the transaction
 * if cannot get all locks, get as much as possible then use wait-die protocal.
 */
void TransactionManager::write(Operation & operation) {
	T_id t_id = operation.t_id;
	I_id item_id = operation.item_id;

	if(!isActive(t_id) || isRO(t_id))
		return;
	getWLocks(t_id, item_id); 
	if(hasAllWLocks(t_id, item_id)) {
		recordWrite(t_id, item_id, operation.value);
	}else if(shouldWait(t_id, item_id, false)) { // false means write
		enque(operation);
	}else abort(t_id, false); //false means wait_die

}

/*
 * get transaction status
 */
void TransactionManager::getTranstatus() const{
	for(int i = 0; i < (int)trans.size(); i++) {
		std::cout << "T" << trans[i].t_id << " ";
		switch (trans[i].status) {
			case ACTIVE:
				std::cout << "is active." <<endl;
				break;
			case WAITING:
				std::cout << "is still waiting." << endl;
				break;
			case COMMITTED:
				std::cout << "committed." <<endl;
				break;
			case SITEFAIL:
				std::cout << "aborted due to site fail." <<endl;
				break;
			case WAITDIE:
				std::cout << "aborted due to wait-or-die." <<endl;
				break;
			default:
				std::cout <<"unknow status, something must be wrong." <<endl;
				break;
		}
	}
}

/*
 * wait and die protocol
 * if one transaction try to get locks hold by older transaction, abort young one
 * if the locks hold by younger transaction, older one wait for the lock release 
 */
bool TransactionManager::shouldWait(T_id t_id, I_id item_id, bool isRead) const {
	std::vector<pair<T_id, bool> > conflictTrans = getAllTrans(item_id);
	for (int i = 0; i < (int)conflictTrans.size(); i++) {
		T_id trans2= conflictTrans[i].first;
		if(trans2 == t_id) {
			continue;
		}
		bool isRead2 = conflictTrans[i].second;
		if(!shouldWait(t_id, isRead, trans2, isRead2)) {
			return false;
		}
	}
	
	if(transQue.find(item_id) != transQue.end()) {
		std::deque<std::pair<T_id, bool> >  Q = transQue.find(item_id)->second;
		std::deque<std::pair<T_id, bool> >::iterator it = Q.begin();
		for(; it != Q.end(); it++) {
			if(!shouldWait(t_id, isRead, it->first, it->second)) {
				return false;
			}
		}
	}
	return true;
}

/*
 * compare transaction start time to tell if the transaction should wait
 */
bool TransactionManager::shouldWait(T_id currTrans, bool isRead1, T_id transInline, bool isRead2) const {
	if(currTrans == transInline) {
		return true;
	}
	if(isRead1 && isRead2)
		return true;
	
	if(getStartTime(currTrans) < getStartTime(transInline))
		return true;
	return false;
}

// void TransactionManager::completeRW(Operation & operation) {
// 	T_id t_id = operation.t_id;
// 	I_id item_id = operation.item_id;
// 	bool isRead = operation.isRead;
// 	Val value = operation.value;
// 	if(isRead) {
// 		recordRead(t_id, item_id, value);
// 		std::cout << "T" << t_id << " read x" << item_id << " = " << value << endl;
// 	} else recordWrite(t_id, item_id, value);
// }

/*
 * print read item value
 */
void TransactionManager::printRead(Operation & op, S_id site_id) {
	if(op.isRead) {
		std::cout << "T" << op.t_id << " read x" << op.item_id << " = " << op.value;
		std::cout << " from site " << site_id << endl;
	} 
}

/*
 * get all write locks from all site
 */
void TransactionManager::getWLocks(T_id t_id, I_id item_id) {
	if(item_sites.find(item_id) == item_sites.end()) {
		return;
	}
	std::vector<S_id> sites = item_sites[item_id];
	for(int i = 0; i < (int)sites.size(); i++) {
		 if(DM->setWLock(sites[i], item_id, t_id)) {
		 	setWriteLog(t_id, item_id, sites[i]);
			addSite_trans(sites[i], t_id);
		 }	 	
	}

}

/*
 * get one read lock from any site
 */
bool TransactionManager::getRLock(T_id t_id, I_id item_id){
	if(item_sites.find(item_id) == item_sites.end()) {
		return false;
	}
	std::vector<S_id> sites = item_sites[item_id];
	for(int i = 0; i < (int)sites.size(); i++) {
		 if(DM->setRLock(sites[i], item_id, t_id)) {
		 	setReadLog(t_id, item_id, sites[i]);
			addSite_trans(sites[i], t_id);
		 	return true;
		 }	 	
	}

	return false;
}

/*
 * 
 */
std::vector<std::pair<T_id, bool> > TransactionManager::getAllTrans(I_id item_id) const {
	std::vector<std::pair<T_id, bool> > ans;
	if(item_sites.count(item_id) == 0) {
		return ans;
	}
	std::vector<S_id> sites = item_sites.find(item_id)->second;
	for(int i = 0; i < (int)sites.size(); i++) {
		std::vector<std::pair<T_id, bool> > tempV = DM->getLockOwners(sites[i], item_id);
		std::vector<std::pair<T_id, bool> >:: iterator it = ans.end();
		ans.insert(it, tempV.begin(), tempV.end());
	}
	return ans;
}

/*
 * add waiting transactions to one waiting queue
 */
void TransactionManager::enque(Operation & operation){
	T_id t_id = operation.t_id;
	int i = getIndex(t_id);
	trans[i].pendingOp = operation;
	trans[i].status = WAITING;

	bool isRead = operation.isRead;
	I_id item_id = operation.item_id;
	std::deque<std::pair<T_id, bool> > & transInline = transQue[item_id];
	transInline.push_back(std::pair<T_id, bool>(t_id, isRead));
}

/*
 * active waiting queue when some locks released
 */
void TransactionManager::activateQue(I_id item_id) {
	// std::cout << "in activateQue" << endl;
	if(transQue.empty() || transQue.find(item_id) == transQue.end() || transQue[item_id].empty())
		return;
	std::deque<std::pair<T_id, bool> > & transInline = transQue[item_id];

	
	while(!transInline.empty() && transInline.front().second) {   // true means read
		T_id t_id = transInline.front().first;
		int index = getIndex(t_id);
		Operation & op = trans[index].pendingOp;

		if (!getRLock(t_id, item_id)) {
			return;
		}	 
		trans[index].status = ACTIVE;	 
		op.start = -1; // an invalid sign of the pending operation
		op.value = DM->getValue(item_id, * time_ptr);
		S_id site_id = findSiteAccess(t_id, item_id);
		printRead(op, site_id);
		transInline.pop_front();
	}

	if(!transInline.empty() && !transInline.front().second) { // false means write
		T_id t_id = transInline.front().first;
		int index = getIndex(t_id);
		Operation & op = trans[index].pendingOp;

		getWLocks(t_id, item_id); 
		if(hasAllWLocks(t_id, item_id)) {
			trans[index].status = ACTIVE;
			recordWrite(t_id, item_id, op.value);
			transInline.pop_front();
		}
	}

	if(transInline.empty()) {
		transQue.erase(item_id);
	}
}

/*
 * add waiting readonly transactions to wait only queue
 */
void TransactionManager::enqueRO(Operation & operation) {
	T_id t_id = operation.t_id;
	int i = getIndex(t_id);
	trans[i].pendingOp = operation;
	trans[i].status = WAITING;

	I_id item_id = operation.item_id;
	std::vector<T_id> & t_ids = ROtransQue[item_id];
	t_ids.push_back(t_id);
}

/*
 * clear readonly waiting queue
 */
void TransactionManager::clearROQue(S_id site_id, I_id item_id) {
	if(ROtransQue.find(item_id) == ROtransQue.end())
		return;
	std::vector<T_id> & t_ids = ROtransQue[item_id];
	for(int i = 0; i < (int)t_ids.size(); i++) {
		int index = getIndex(t_ids[i]);
		trans[index].status = ACTIVE; 
		trans[index].pendingOp.value = getSnapshotVal(t_ids[i], item_id);
		printRead(trans[index].pendingOp, site_id);
	}
	ROtransQue.erase(item_id);
}

/*
 * find anysite have one paticular Item is up 
 */
S_id TransactionManager::hasSiteUp(I_id item_id) const {
	if(item_sites.find(item_id) == item_sites.end()) {
		return -1;
	}
	std::vector<S_id> sites = item_sites.find(item_id)->second;
	for(int i = 0; i < (int)sites.size(); i++) {
		if(site_status.find(sites[i]) != site_status.end() && site_status.find(sites[i])->second)  {
			return sites[i];
		}
	}
	return -1;
}

/*
 * tell if one transaction have locks of one Item
 */
bool TransactionManager::hasSiteAccess(T_id t_id, I_id item_id) const {
	int i = getIndex(t_id);
	std::map<I_id, std::list<SiteAccess> > access_log = trans[i].access_log;
	if(access_log.count(item_id) == 0 || access_log[item_id].empty()) {
		return false;
	}
	return true;
}

/*
 * 
 */
S_id TransactionManager::findSiteAccess(T_id t_id, I_id item_id) const {
	int i = getIndex(t_id);
	try {
		std::map<I_id, std::list<SiteAccess> > access_log = trans[i].access_log;
		if(hasSiteAccess(t_id, item_id)) {
			return access_log[item_id].rbegin()->site_id; 
				
		} else {
			throw "No access to some variable";
		}
	}catch(const char* msg) {
        std::cerr << msg << endl;
    }
	return -1;
}

/*
 * 
 */
bool TransactionManager::isReplicated(I_id item_id) const {
	if(item_sites.find(item_id) == item_sites.end())
		return false;
	if(item_sites.find(item_id)->second.size() <= 1)
		return false;
	return true;
}

/*
 * release locks
 */
std::set<I_id> TransactionManager::releaseLockNLog(T_id t_id) {
	// std::cout << "In releaseLockNLog" << endl;
	int i = getIndex(t_id);
	std::map<I_id, std::list<SiteAccess> > & access_log = trans[i].access_log;
	std::set<I_id> items;
	std::map<I_id, std::list<SiteAccess> >::iterator it = access_log.begin();
	for(; it != access_log.end(); it++) {
		I_id item_id = it->first;
		items.insert(item_id);
		std::list<SiteAccess> & access = it->second;
		std::list<SiteAccess>::iterator it2 = access.begin();
		for(; it2 != access.end(); it2++) {
			S_id site_id = it2->site_id;
			removeSite_trans(site_id, t_id);
			DM->removeLock(site_id, item_id, t_id);
		}
	}
	access_log.clear();
	return items;
}

void TransactionManager::setReadLog(T_id t_id, I_id item_id, S_id site_id) {
	int i = getIndex(t_id);
	std::list<SiteAccess> & accessList = trans[i].access_log[item_id];
	std::list<SiteAccess>::reverse_iterator rit = accessList.rbegin();
	for(; rit != accessList.rend(); rit++) {
		if(rit->site_id == site_id) {
			return;
		}
	}
	SiteAccess access;
	access.isRead = true;
	access.site_id = site_id;
	access.initial_time = * time_ptr;
	accessList.push_back(access);
}

void TransactionManager::setWriteLog(T_id t_id, I_id item_id, S_id site_id) {
	int i = getIndex(t_id);
	std::list<SiteAccess> & accessList = trans[i].access_log[item_id];
	std::list<SiteAccess>::reverse_iterator rit = accessList.rbegin();
	for(; rit != accessList.rend(); rit++) {
		if(rit->site_id == site_id && !rit->isRead) {
			return;
		}
	}
	SiteAccess access;
	access.isRead = false;
	access.site_id = site_id;
	access.initial_time = * time_ptr;
	accessList.push_back(access);
}


void TransactionManager::recordWrite(T_id t_id, I_id item_id, Val value) {
	int i = getIndex(t_id);
	trans[i].write_or_snapshot[item_id] = value;
}


bool TransactionManager::hasWritten(T_id t_id, I_id item_id) const {
	int i = getIndex(t_id);
	if(trans[i].isRO) {
		return false;
	}

	if(trans[i].write_or_snapshot.find(item_id) == trans[i].write_or_snapshot.end()) {
		return false;
	}
	return true;
}

/*
 * tell if one transaction have all writelocks of one Item
 */
bool TransactionManager::hasAllWLocks(T_id t_id, I_id item_id) const {
	bool hasSomeWLock = false;
	if(item_sites.find(item_id) == item_sites.end())
		return false;
	std::vector<S_id> sites = item_sites.find(item_id)->second; 
	for(int i = 0; i < (int)sites.size(); i++) {
		if(site_status.count(sites[i]) && site_status.find(sites[i])->second) {
			if (!DM->hasWLock(sites[i], item_id, t_id)) {
				return false;
			} else hasSomeWLock = true;
		}   
	}
	return hasSomeWLock;
}

/*
 * get readonly transaction some Item snapshot value
 */
Val TransactionManager::getSnapshotVal(T_id t_id, I_id item_id) const {
	int i = getIndex(t_id);
	try {
		if(trans[i].isRO && trans[i].write_or_snapshot.find(item_id) != trans[i].write_or_snapshot.end()) {
			return trans[i].write_or_snapshot.find(item_id)->second;
		}
		else throw "Cannot find value of some item in the snapshot of the readonly transcation";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return -100;
}

Val TransactionManager::getWVal(T_id t_id, I_id item_id) const {
	int i = getIndex(t_id);
	try {
		if(!trans[i].isRO && trans[i].write_or_snapshot.find(item_id) != trans[i].write_or_snapshot.end()) {
			return trans[i].write_or_snapshot.find(item_id)->second;
		}
		else throw "Cannot find value of some item in writes of some transcation";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return -100;
}

bool TransactionManager::isActive(T_id t_id) const {
	int i = getIndex(t_id);
	if(trans[i].status == ACTIVE)
		return true;
	return false;
}

bool TransactionManager::isWaiting(T_id t_id) const {
	int i = getIndex(t_id);
	if(trans[i].status == WAITING)
		return true;
	return false;
}

bool TransactionManager::isAborted(T_id t_id) const {
	int i = getIndex(t_id);
	if(trans[i].status == SITEFAIL || trans[i].status == WAITDIE)
		return true;
	return false;

}

bool TransactionManager::isCommitted(T_id t_id) const {
	int i = getIndex(t_id);
	if(trans[i].status == COMMITTED)
		return true;
	return false;
}

bool TransactionManager::isRO(T_id t_id) const {
	int i = getIndex(t_id);
	return trans[i].isRO;
}

/*
 * get transaction start time
 */
s_time TransactionManager::getStartTime(T_id t_id) const{
	int i = getIndex(t_id);
	return trans[i].start;
}

/*
 * get transaction inex
 */
int TransactionManager::getIndex(T_id t_id) const {
	int i = -1;
	if(trans_index.find(t_id) != trans_index.end()) {
		i = trans_index.find(t_id)->second;	
	} 
	try {
		if(i >= 0 && i < (int)trans.size())
			return i;
		else throw "Cannot find the index of the transcation";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return 0;	
}

/*
 * add site to transaction information
 */
void TransactionManager::addSite_trans(S_id site_id, T_id t_id) {
	std::set<T_id> & t_ids = site_trans[site_id];
	t_ids.insert(t_id);
}

/*
 * remove all transactions in one site
 */
void TransactionManager::removeSite_trans(S_id site_id, T_id t_id) {
	// std::cout << "In removeSite_trans" <<endl;
	if(site_trans.empty() || site_trans.find(site_id) == site_trans.end())
		return;
	std::set<T_id> & t_ids = site_trans[site_id];
	if(t_ids.find(t_id) != t_ids.end()) {
		t_ids.erase(t_id);
	}
	if(t_ids.size() == 0) {
		site_trans.erase(site_id);
	}
}




