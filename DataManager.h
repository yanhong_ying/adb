//
//  DataManager.h
//  
//
//  Created by Ying Zhang and Yanhong Yang on 12/4/15.
//
//

#ifndef ADB_DATAMANAGER_H
#define ADB_DATAMANAGER_H

 
#include "cons.h"

using namespace std;

class DataManager {
private:
    s_time * time_ptr;
	vector<Site> sites;
	map<S_id, int> site_index;

    
    vector<Item> data;
    map<I_id, int> item_index;

    map<I_id, vector<S_id> > item_sites;

public:
	DataManager(s_time * time_ptr, vector<Site> &sites, map<S_id, int> & site_index, vector<Item> & data, map<I_id, int> & item_index, 
		map<I_id, vector<S_id> > & item_sites)
    : time_ptr(time_ptr), sites(sites), site_index(site_index), data(data), item_index(item_index), item_sites(item_sites){}

	map<I_id, vector<S_id> >  initial_item_sites() const {
    	return item_sites;
    }  
    map<S_id, vector<I_id> > initial_site_items() const;  
    map<S_id, bool>  initial_site_status() const;

    bool contain(S_id, I_id) const;

    Val getValue(I_id, s_time) const;
    void addCommit(I_id, Val);
    void reactivateRep(S_id, I_id);

    vector<pair<T_id, bool> > getLockOwners(S_id, I_id) const; // true means isRead
     
    bool setRLock(S_id, I_id, T_id);
    bool setWLock(S_id, I_id, T_id);
    bool hasWLock(S_id, I_id, T_id) const;
    void removeLock(S_id, I_id, T_id);
    

    void fail(S_id);
    void recover(S_id);

    void dumpSite(S_id) const;
    void dumpItem(I_id) const;
    void dump() const;
    Val show(I_id, S_id) const;

    int getS_Index(S_id) const;
    int getI_Index(I_id) const;
};

#endif /* ADB_DATAMANAGER_H */
