//
//  DataManager.cpp
//  
//
//  Created by Ying Zhang and Yanhong Yang on 12/4/15.
//
//

#include "DataManager.h"

/*
 * construct data manager 
 */
std::map<S_id, std::vector<I_id> > DataManager::initial_site_items() const {
	std::map<S_id, std::vector<I_id> > ans;

	for(int i = 0; i < (int)sites.size(); i++) {
		std::vector<I_id> tempV;
		tempV.insert(tempV.begin(), sites[i].items.begin(), sites[i].items.end());
		ans[sites[i].site_id] = tempV;	 
	}
	return ans;
}

/*
 * init site 
 */
std::map<S_id, bool>  DataManager::initial_site_status() const{
	std::map<S_id, bool> ans;
	for(int i = 0; i < (int)sites.size(); i++) {
		 ans[sites[i].site_id] = true;
	}
	return ans;
}

/*
 * tell if one site have some Item
 */
bool DataManager::contain(S_id site_id, I_id item_id) const{
	int s_index = getS_Index(site_id);
	if(sites[s_index].items.count(item_id) == 0)
		return false; 
	return true;
}

/*
 * get Item value from database
 */
Val DataManager::getValue(I_id item_id, s_time currtime) const {
	int i = getI_Index(item_id);
	std::deque <PTV>  records = data[i].records;
	std::deque <PTV>::reverse_iterator rit = records.rbegin();
	while(rit != records.rend() && rit->first > currtime) {
		rit++;
	}
	try {
		if(rit != records.rend() && rit->first <= currtime)
			return rit->second;
		else throw "the time is not correct";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return 0;
}
    
void DataManager::addCommit(I_id item_id, Val value) {
	int i = getI_Index(item_id);
	std::deque <PTV> & records = data[i].records;
	records.push_back(std::pair<s_time, Val> (* time_ptr, value));
}


void DataManager::reactivateRep(S_id site_id, I_id item_id) {
	if(!contain(site_id, item_id))
		return;
	int s_index = getS_Index(site_id);
	std::unordered_map<I_id, std::pair<s_time, bool> > &rep_items = sites[s_index].rep_items;
	if(rep_items.find(item_id) != rep_items.end() && !rep_items[item_id].second) {
		rep_items[item_id] = std::pair<s_time, bool>(* time_ptr, true);
	}
}

/*
 * get locks transaction owners
 */
std::vector<std::pair<T_id, bool> > DataManager::getLockOwners(S_id site_id, I_id item_id) const {
	std::vector<std::pair<T_id, bool> > ans;
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp || sites[s_index].lockTable.find(item_id) == sites[s_index].lockTable.end())
		return ans;
	std::list<Lock> locks = sites[s_index].lockTable.find(item_id)->second;
	std::list<Lock>::iterator it = locks.begin();
	for(; it != locks.end(); it++) {
		ans.push_back(std::pair<T_id, bool>(it->t_id, it->isRLock));
	}
	return ans;
}
     
/*
 * set read lock of one item in one site
 */
bool DataManager::setRLock(S_id site_id, I_id item_id, T_id t_id) {
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp || !contain(site_id, item_id)) {
		return false;
	}
	std::unordered_map<I_id, std::pair<s_time, bool> > &rep_items = sites[s_index].rep_items;
	if(rep_items.find(item_id) != rep_items.end() && !rep_items[item_id].second) {	
		return false;
	}

	std::list<Lock> & locks = sites[s_index].lockTable[item_id];
	std::list<Lock>::iterator it = locks.begin();
	for(; it != locks.end(); it++) {
		if(it->t_id != t_id && !it->isRLock) {
			return false;
		} else if (it->t_id == t_id) {
			return true;
		}
	}
	Lock currLock;
	currLock.t_id = t_id;
	currLock.isRLock = true;
	currLock.lockTime = * time_ptr;
	locks.push_back(currLock);
	return true;
}

/*
 * set write lock from one item in one site
 */
bool DataManager::setWLock(S_id site_id, I_id item_id, T_id t_id) {
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp || !contain(site_id, item_id)) {
		return false;
	}
	std::list<Lock> & locks = sites[s_index].lockTable[item_id];
	std::list<Lock>::iterator it = locks.begin();
	for(; it != locks.end(); it++) {
		if(it->t_id != t_id) {
			return false;
		} else if (!it->isRLock) {
			return true;
		}
	}
	Lock currLock;
	currLock.t_id = t_id;
	currLock.isRLock = false;
	currLock.lockTime = * time_ptr;
	locks.push_back(currLock);
	return true;
}

/*
 * tell if some site have Item lock
 */
bool DataManager::hasWLock(S_id site_id, I_id item_id, T_id t_id) const{
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp || !contain(site_id, item_id) || sites[s_index].lockTable.count(item_id) == 0) {
		return false;
	}
	std::list<Lock>  locks = sites[s_index].lockTable.find(item_id)->second;
	std::list<Lock>::iterator it = locks.begin();
	for(; it != locks.end(); it++) {
		if(it->t_id == t_id && !it->isRLock) {
			return true;
		}
	}
	return false;
}
 
/*
 * remove locks when transaction abort or commit
 */ 
void DataManager::removeLock(S_id site_id, I_id item_id, T_id t_id) {
	// cout << "In DM-> removeLock" << endl;
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp || !contain(site_id, item_id) || sites[s_index].lockTable.empty()
		|| sites[s_index].lockTable.count(item_id) == 0) {
		return;
	}
	std::list<Lock> & locks = sites[s_index].lockTable[item_id];
	std::list<Lock>::iterator it = locks.begin();
	for(; it != locks.end(); ) {
		if(it->t_id == t_id) {
			it = locks.erase(it);
		} else it++;
	}
}
   
/*
 * clear site all data and lock table when its fail
 */
void DataManager::fail(S_id site_id) {
	int s_index = getS_Index(site_id);
	sites[s_index].isUp = false;
	// sites[s_index].lockTable.clear();
	sites[s_index].siteLog.push_back(std::pair<s_time, bool>(* time_ptr, false));
	std::unordered_map<I_id, std::pair<s_time, bool> > & rep_items = sites[s_index].rep_items;
	std::unordered_map<I_id, std::pair<s_time, bool> >::iterator it = rep_items.begin();
	for(; it != rep_items.end(); it++) {
		it->second = std::pair<s_time, bool>(* time_ptr, false);
	}
}

/*
 * recover site.
 */
void DataManager::recover(S_id site_id) {
	int s_index = getS_Index(site_id);
	sites[s_index].isUp = true;
	sites[s_index].siteLog.push_back(std::pair<s_time, bool>(* time_ptr, true));
}

/*
 * dump all values on one site
 */
void DataManager::dumpSite(S_id site_id) const {
	int s_index = getS_Index(site_id);
	if(!sites[s_index].isUp) {
		std::cout << "site " << site_id << " is down." << endl;
	}else {
		std::cout << "site " << site_id  << ":" << endl;
		std::set<I_id>  items = sites[s_index].items;
		std::set<I_id>::iterator it = items.begin();
		for(; it != items.end(); it++) {
			std::cout << "x" << *it <<" = " << show(*it, site_id) << endl;;
		}
	}
}

/*
 * dump some values on one site
 */
void DataManager::dumpItem(I_id item_id) const {
	if(item_sites.find(item_id) == item_sites.end()) {
		std::cout << "Cannot find x" << item_id << endl;
	}
	std::vector<S_id>  s_ids = item_sites.find(item_id)->second;
	std::cout << "x" << item_id << ":" << endl;
	for(int i = 0; i < (int)s_ids.size(); i++) {
		int s_index = getS_Index(s_ids[i]);
		if(sites[s_index].isUp) {
	 		std::cout << show(item_id, s_ids[i]) << " at site "<<s_ids[i]<<";" << endl;
	 	} 
	 	// else cout << " is down. ";
	}
	std::cout << endl;
}

void DataManager::dump() const {
	for(int i = 0; i < (int)sites.size(); i++) {
		dumpSite(sites[i].site_id);
		std::cout << endl;
	}
}

Val DataManager::show(I_id item_id, S_id site_id) const {
	int s_index = getS_Index(site_id);
	try {
		if(!sites[s_index].isUp || !contain(site_id, item_id)) {
			throw "site is down or does not contain a copy of the variable";
		}

		std::unordered_map<I_id, std::pair<s_time, bool> > rep_items = sites[s_index].rep_items;
		s_time valid_time = * time_ptr;
		if(rep_items.find(item_id) != rep_items.end() && !rep_items.find(item_id)->second.second) {	
			valid_time = rep_items.find(item_id)->first;
		}
		return getValue(item_id, valid_time);
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return -1;
}

/*
 * get site index
 */
int DataManager::getS_Index(S_id site_id) const{
	int i = -1;
	if(site_index.find(site_id) != site_index.end()) {
		i = site_index.find(site_id)->second;	
	} 
	try {
		if(i >= 0 && i < (int)sites.size())
			return i;
		else throw "Cannot find the index of the site";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return 0;	
}

/*
 * get Item index 
 */    
int DataManager::getI_Index(I_id item_id) const{
	int i = -1;
	if(item_index.find(item_id) != item_index.end()) {
		i = item_index.find(item_id)->second;	
	} 
	try {
		if(i >= 0 && i < (int)data.size())
			return i;
		else throw "Cannot find the index of the site";
	} catch(const char* msg) {
        std::cerr << msg << endl;
    }
    return 0;	
}

 
