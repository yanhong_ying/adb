//
// Created by Ying Zhang and Yanhong Yang on 12/1/15.
//

#include "DBSystem.h"


#include <stdlib.h> // atoi
#include <ctype.h> //isdigit
#include <algorithm> //remove


/*
 * construct of DBSystem
 * set up system time of DBSystem
 */
DBSystem::DBSystem(std::vector<Site> & sites, std::map<S_id, int> & site_index, std::vector<Item> & data, 
	std::map<I_id, int> & item_index, std::map<I_id, std::vector<S_id> > & item_sites)
: DM(& currtime, sites, site_index, data, item_index, item_sites), TM(& currtime, & DM) {
	currtime = 0;    
}


/*
 * handle commands read frome file
 * skip comments start with "//"
 * 
 */

void DBSystem::handleCommands(std::string line) {
    currtime++;
    line = trim(line);
    if((int)line.length() == 0 || line.substr(0, 2) == "//") {
    	return;
    }
  	
    std::vector<string> commands = lineToCommands(line);
    for(int i = 0; i < (int)commands.size(); i++) {
    	runCommand(commands[i]);
    }
}

/*
 * run command based on opertion type
 * operation type : begin, read, write, end, dump, fail, recover
 * 
 */
void DBSystem::runCommand(std::string command) {
	std::string op = command.substr(0, command.find("("));
	Optype optype = identifyOperationType(op);
	switch (optype) {
		case BEGIN:
			beginTrans(command);
			break;
		case BEGINRO:
			beginROTrans(command);
			break;
		case R:
			read(command);
			break;
		case W:
			write(command);
			break;
		case END:
			endTrans(command);
			break;
		case DUMP:
			dump(command);
			break;
		case FAIL:
			fail(command);
			break;
		case RECOVER:
			recover(command);
			break;
		default:
			std::cout << "Invalid operation: " << command << endl;
			break;
	}
}

/*
 * begin transaction.
 * call TransactionManager begin(t_id)
 */
void DBSystem::beginTrans(std::string command) {
	T_id t_id = getNums(command);
	if(t_id == -1) {
		std::cout << "wrong transaction id" << endl;
	}
	TM.begin(t_id);
}

/*
 * begin read only transaction
 */
void DBSystem::beginROTrans(std::string command) {
	T_id t_id = getNums(command);
	if(t_id == -1) {
		std::cout << "wrong transaction id" << endl;
	}
	TM.beginRO(t_id);
}

/*
 * create read operation
 */
void DBSystem::read(std::string command) {
	Operation op;
	op.isRead = true;
	op.t_id = getNums(command);
	
	int i = command.find('x');
	op.item_id = getNums(command.substr(i));
	if(op.t_id == -1 || op.item_id == -1) {
		std::cout << "Wrong read command" << endl;
	}
	op.start = currtime;
	TM.read(op);
}

/*
 * create write operation
 */
void DBSystem::write(std::string command) {
	Operation op;
	op.isRead = false;
	op.t_id = getNums(command);
	 
	int i = command.find('x');
	command = command.substr(i);
	op.item_id = getNums(command);
	if(op.t_id == -1 || op.item_id == -1) {
		std::cout << "Wrong write command" << endl;
	}

	i = command.find(',');
	op.value = getNums(command.substr(i));
	op.start = currtime;
	TM.write(op);
}

/*
 * end transaction
 */
void DBSystem::endTrans(std::string command){
	T_id t_id = getNums(command);
	if(t_id == -1) {
		std::cout << "wrong transaction id" << endl;
	}
	TM.end(t_id);
}

/*
 * dump whole database or single Item in database
 * 
 */
void DBSystem::dump(std::string command) const {
	if(command.find('x') != std::string::npos) {
		I_id item_id = getNums(command);
		DM.dumpItem(item_id);
	}else {
		bool dumpSite = false;
		for (int i = (int)command.find('('); i < (int)command.find(')'); i++) {
			if(isdigit(command[i])){
				dumpSite = true;
				break;
			}
		}

		if(dumpSite) {
			S_id site_id = getNums(command);
			DM.dumpSite(site_id);
		}else DM.dump();
	}
}

/*
 * fail one site
 */
void DBSystem::fail(std::string command) {
	S_id site_id = getNums(command);
	if(site_id == -1) {
		std::cout << "wrong site id" << endl;
	}
	DM.fail(site_id);
	TM.fail(site_id);
}

/*
 * recover failed site
 */
void DBSystem::recover(std::string command) {
	S_id site_id = getNums(command);
	if(site_id == -1) {
		std::cout << "wrong site id" << endl;
	}
	DM.recover(site_id);
	TM.recover(site_id);
}


void DBSystem::queryTranstatus() const {
	TM.getTranstatus();
}

void DBSystem::queryDBstatus() const {
	DM.dump();
}

/*
 * identify operation type
 */
Optype DBSystem::identifyOperationType (std::string op) {
	if(op == "begin")
		return BEGIN;
	if(op == "beginro")
		return BEGINRO;
	if(op == "r")
		return R;
	if(op == "w")
		return W;
	if(op == "end")
		return END;
	if(op == "dump")
		return DUMP;
	if(op == "fail")
		return FAIL;
	if(op == "recover")
		return RECOVER;
	return INVALID;	
}

/*
 * trim start and end spaces of string
 */
std::string DBSystem::trim(std::string str) const {
    str.erase(remove(str.begin(), str.end(),' '), str.end());
    return str;
}

/*
 * translate line read from file to commands
 * every line may have several commands seperated by ";"
 * commands in same line have same s_time
 */
std::vector<std::string> DBSystem::lineToCommands(std::string line) const {
	std::vector<std::string> ans;
	int start = 0, end = 0;
	line += ";";

    while((end = (int)line.find(";", start)) != std::string::npos) {
		std::string tmp = trim(line.substr(start, end-start));
    	std::string r;
        for(int i = 0; i < (int)tmp.size(); i++){
            r += tolower(tmp[i]);
        }
		if((int)r.size() > 0){
        	ans.push_back(r);
        }
        start = end + 1;
    }
    return ans;
}

/*
 * get number from command
 */
int DBSystem::getNums (std::string command) const{
	int start = -1;
	int end = (int)command.length();
	for (int i = 0; i < (int)command.length(); i++) {
		if(isdigit(command[i])) {
			if(start == -1) 
				start = i;
		} else if (start >= 0) {
			end = i;
			break;
		}
	}
	try {
		if (start >= end || start == -1){
			throw "cannot get a number";
		}else return atoi(command.substr(start, end - start).c_str());
			
	}catch(const char* msg) {
        cerr << msg << endl;
    }
    return -1;
}
 
